import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ContactosComponent } from './contactos/contactos.component';
import { GaleriaComponent } from './galeria/galeria.component';



@NgModule({
  declarations: [
   
    InicioComponent,
    ServiciosComponent,
    ContactosComponent,
    GaleriaComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
